const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const userRoutes = require('./routes/user.routes');
require('dotenv').config({ path: './config/.env' })
require('./config/db');
const { checkUser, requireAuth } = require('./middleware/auth.middleware');
//middlewares
//Pour lire le json
app.use(bodyParser.json());
//Pour lire les url
app.use(bodyParser.urlencoded({ extended: true }));
//Pour lire les cookies
app.use(cookieParser());

// jwt
//* = pour toutes les routes tu me dis si l'utilisateur est valide
//Vérifie que l'utilisateur est bien connecté et bien identifié
app.get('*', checkUser);
//Authentification à la connexion
app.get('/auth', requireAuth, (req, res) => {
    res.status(200).send(res.locals.user._id)
})


//routes
app.use('/api/user', userRoutes)

//création du serveur
app.listen(process.env.PORT, () => {
    console.log(`Listening on port ${process.env.PORT}`);
});