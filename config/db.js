const mongoose = require("mongoose");

mongoose
    .connect(
        "mongodb+srv://" + process.env.DB_USER_PASS + "@cluster0.zz2jeqx.mongodb.net/test"
    )
    .then(() => console.log("connecté à Mongodb"))
    .catch((err) => console.log("Failed to connect to MongoDB", err));
