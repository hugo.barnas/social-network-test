//inscription
const UserModel = require('../models/user.model');
const jwt = require('jsonwebtoken');
const { signUpErrors } = require("../utils/errors.utils");

//durée de vie du cookie
const maxAge = 3 * 21 * 60 * 60 * 1000;

//Génération d'un token permettant à l'utilisateur d'aller sur tout le site
const createToken = (id) => {
    return jwt.sign({ id }, process.env.TOKEN_SECRET, {
        expiresIn: maxAge
    })
};


//inscription
module.exports.signUp = async (req, res) => {
    //insertion des informations demandées
    const { pseudo, email, password } = req.body;
    console.log("req.body :", req.body);

    try {
        const user = await UserModel.create({ pseudo, email, password });
        //Renvoi d'une réponse avec un id
        res.status(201).json({ user: user._id })
    }
    catch (err) {
        const errors = signUpErrors(err);
        res.status(400).send({ errors })
    }
}


//connexion
module.exports.signIn = async (req, res) => {
    const { email, password } = req.body
    try {
        const user = await UserModel.login(email, password);
        //Création d'un token
        const token = createToken(user._id);
        res.cookie('jwt', token, { httpOnly: true, maxAge });
        res.status(200).json({ user: user._id })
    }
    catch (err) {
        res.status(400).json({ err })
    }
}

//Sortie
module.exports.logout = (req, res) => {
    res.cookie('jwt', '', { maxAge: 1 });
    res.redirect('/');
}
