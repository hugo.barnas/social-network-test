const UserModel = require("../models/user.model");
const ObjectID = require('mongoose').Types.ObjectId;


//Récupération de tous les utilisateurs
module.exports.getAllUsers = async (req, res) => {
    //suppression du mot de passe au moment de la récupération des users sans le mot de passe
    const users = await UserModel.find().select('-password').select('-email');
    res.status(200).json(users);
}

//Récupération d'un utilisateur
module.exports.userInfo = (req, res) => {
    console.log(req.params);
    if (!ObjectID.isValid(req.params.id)) {
        return res.status(400).send("l'id de cet utilisateur n'existe pas : " + req.params.id)
    } else {
        UserModel.findById(req.params.id, (err, docs) => {
            if (!err) {
                res.send(docs);
            }
            else {
                console.log('id inconnu : ' + err)
            }
        }).select('-password')
    }
}

//Mise à jour d'un user
module.exports.updateUser = async (req, res) => {
    if (!ObjectID.isValid(req.params.id))
        return res.status(400).send("ID invalid : " + req.params.id);

    try {
        await UserModel.findOneAndUpdate(
            { _id: req.params.id },
            {
                $set: {
                    bio: req.body.bio,
                },
            },
            { new: true, upsert: true, setDefaultsOnInsert: true }
        )
            .then((docs) => res.send(docs))
            .catch((err) => res.status(500).send({ message: err }));
    } catch (err) {
        res.status(500).json({ message: err });
    }
};


//Suppression d'un user
module.exports.deleteUser = async (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send("l'id de cet utilisateur n'existe pas : " + req.params.id)
    try {
        await UserModel.deleteOne({ _id: req.params.id }).exec();
        res.status(200).json({ message: "Suppression réussie" })

    }
    catch (err) {
        return res.status(500).json({ message: err })
    }
}

//Ajout d'un follower
module.exports.follow = (req, res) => {
    if (
        !ObjectID.isValid(req.params.id) ||
        !ObjectID.isValid(req.body.idToFollow)
    )
        return res.status(400).send("Idtofollow non connu");

    try {
        // add to the follower list
        UserModel.findByIdAndUpdate(
            req.params.id,
            { $addToSet: { following: req.body.idToFollow } },
            { new: true, upsert: true },
            (err, docs) => {
                if (!err) res.status(201).json(docs);
                if (err) return res.status(400).jsos(err);
            }
        );
        // add to following list
        UserModel.findByIdAndUpdate(
            req.body.idToFollow,
            { $addToSet: { followers: req.params.id } },
            { new: true, upsert: true },
            (err, docs) => {
                // if (!err) res.status(201).json(docs);
                if (err) return res.status(400).jsos(err);
            }
        );
    } catch (err) {
        return res.status(500).json({ message: err });
    }
};


//Ne plus suivre un user
module.exports.unfollow = async (req, res) => {
    if (
        !ObjectID.isValid(req.params.id) ||
        !ObjectID.isValid(req.body.idToUnfollow)
    )
        return res.status(400).send("ID unknown : " + req.params.id);

    try {
        await UserModel.findByIdAndUpdate(
            req.params.id,
            { $pull: { following: req.body.idToUnfollow } },
            { new: true, upsert: true },
            (err, docs) => {
                if (!err) res.status(201).json(docs);
                else return res.status(400).jsos(err);
            }
        );
        // remove to following list
        await UserModel.findByIdAndUpdate(
            req.body.idToUnfollow,
            { $pull: { followers: req.params.id } },
            { new: true, upsert: true },
            (err, docs) => {
                // if (!err) res.status(201).json(docs);
                if (err) return res.status(400).jsos(err);
            }
        );
    } catch (err) {
        return res.status(500).json({ message: err });
    }
};
