const jwt = require("jsonwebtoken");
const UserModel = require("../models/user.model");

//Authentification de l'utilisateur qd l'utilisateur passe sur chaque route
module.exports.checkUser = (req, res, next) => {
    const token = req.cookies.jwt;
    if (token) {
        jwt.verify(token, process.env.TOKEN_SECRET, async (err, decodedToken) => {
            if (err) {
                //tu ne m'affiches pas les données
                res.locals.user = null;
                //Suppression du cookie
                res.cookie('jwt', '', { maxAge: 1 });
                //Passe à la fonction suivante
                next();
            } else {
                console.log('decoded token :', decodedToken.id);
                let user = await UserModel.findById(decodedToken.id);
                res.locals.user = user;
                //si le cookie est bon alors tu m'affiches les données
                console.log(res.locals.user);
                next();
            }
        })
    } else {
        //Tu ne m'affiches pas les données
        res.locals.user = null;
        next();
    }
};

//Contrôle de l'utilisateur au moment de la connexion
module.exports.requireAuth = (req, res, next) => {
    const token = req.cookies.jwt;
    if (token) {
        jwt.verify(token, process.env.TOKEN_SECRET, async (err, decodedToken) => {
            if (err) {
                console.log(err)
            } else {
                console.log(decodedToken.id);
                next()
            }
        })
    } else {
        console.log('no token')
    }
};