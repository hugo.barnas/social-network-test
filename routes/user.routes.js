//Création d'un utilisateur
const router = require('express').Router();
//Création d'un controller
const authController = require('../controllers/auth.controller');
const userController = require('../controllers/user.controller');

//Authentification
//Route pour ajouter un user
router.post("/register", authController.signUp);
//Route pour s'authentifier
router.post("/login", authController.signIn);
router.get("/logout", authController.logout);

//user CRUD
//Route pour récuper tous les users
router.get('/', userController.getAllUsers);
//Route pour la récupération d'un user
router.get('/:id', userController.userInfo);
//Route pour la mise à jour d'un user
router.put('/:id', userController.updateUser);
//Route pour supprimer un user
router.delete('/:id', userController.deleteUser);
//Route pour ajouter un follower
router.patch('/follow/:id', userController.follow);
//Route pour supprimer un follower
router.patch('/unfollow/:id', userController.unfollow);

module.exports = router;