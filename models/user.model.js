const mongoose = require('mongoose');
const { isEmail } = require('validator');
const bcrypt = require("bcrypt");

//Création d'un module user
const userSchema = new mongoose.Schema(
    {
        pseudo: {
            type: String,
            required: true,
            minLength: 3,
            maxLength: 20,
            unique: true,
            //suppression des espaces
            trim: true
        },
        email: {
            type: String,
            required: true,
            validate: [isEmail],
            unique: true,
            lowercase: true,
            trim: true
        },
        password: {
            type: String,
            required: true,
            minlength: 6
        },
        picture: {
            type: String,
            default: "./uploads/profil/random-user/png"
        },
        bio: {
            type: String,
            maxLength: 1024
        },
        followers: {
            type: [String]
        },
        following: {
            type: [String]
        },
        likes: {
            type: [String]
        },
    },
    {
        //tracking temps permettant de savoir qd l'utilisateur s'est connecté
        timestamps: true
    }
)


//déclaration d'une fonction qui crypte le mot de passe
userSchema.pre("save", async function (next) {
    const salt = await bcrypt.genSalt();
    this.password = await bcrypt.hash(this.password, salt);
    this.email = await bcrypt.hash(this.email, salt);
    next();
})

userSchema.statics.login = async function (email, password) {
    const user = await this.findOne({ email });
    if (user) {
        const auth = await bcrypt.compare(password, user.password)
        if (auth) {
            return user;
        }
        throw Error('Mot de passe incorrect')
    }
    throw Error('email incorrect')
};

//instanciation du modèle
const UserModel = mongoose.model('user', userSchema);

//exportation du modèle
module.exports = UserModel;